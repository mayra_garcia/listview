package com.example.listview

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var frutas:ArrayList<String> = ArrayList()
        frutas.add("Kiwi")
        frutas.add("Mango")
        frutas.add("Manzana")
        frutas.add("Naranja")
        frutas.add("Platano")
        frutas.add("Sandia")
        frutas.add("Uva")

        var listaFrutas = findViewById<ListView>(R.id.lista)
        var adapter =  ArrayAdapter<String>(this,R.layout.items,frutas)

        listaFrutas.adapter = adapter

        listaFrutas.onItemClickListener = AdapterView.OnItemClickListener { adapterView, view, i, l ->
            //Toast.makeText(this,frutas.get(i),Toast.LENGTH_LONG).show()
            if (i == 0){
                val intent = Intent(this,kiwi::class.java)
                startActivity(intent)
                finish()
            }else if (i == 1){
                val intent = Intent(this,mango::class.java)
                startActivity(intent)
                finish()
            }else if (i == 2){
                val intent = Intent(this,manzana::class.java)
                startActivity(intent)
                finish()
            }else if (i == 3){
                val intent = Intent(this,naranja::class.java)
                startActivity(intent)
                finish()
            }else if (i == 4){
                val intent = Intent(this,platano::class.java)
                startActivity(intent)
                finish()
            }else if (i == 5){
                val intent = Intent(this,sandia::class.java)
                startActivity(intent)
                finish()
            }else if (i == 6){
                val intent = Intent(this,uva::class.java)
                startActivity(intent)
                finish()
            }

        }

    }
}
